const DEVTOOLS_RTT_ADJUSTMENT_FACTOR = 3.75;
const DEVTOOLS_THROUGHPUT_ADJUSTMENT_FACTOR = 0.9;
describe('example e2e tests', () => {
    before(() => {
      cy.loginAs("testuser1@qa.com","scaleAQ..") 
      cy.restoreLocalStorage()
    })
  
    beforeEach(() => {
     cy.restoreLocalStorage();
    })

    it.only('Performance Home page', () => {
        cy.visit('/')
        cy.lighthouse(
          {
            performance: 10, 
            "first-contentful-paint": 2000,
            "speed-index": 1000,
            accessibility: 10,
            'best-practices': 10,
            seo: 10,
          },
          {
            formFactor: 'desktop',
            screenEmulation: {
              mobile: false,
              disable: false,
              width: Cypress.config('viewportWidth'),
              height: Cypress.config('viewportHeight'),
              deviceScaleRatio: 1,
            },
          },
        )
      })

      it.skip('Performance Storages', () => {
        cy.visit('/storages')
       // cy.get(':nth-child(2) > .MuiListItemText-root > .MuiTypography-root').click()

        cy.lighthouse(
          {
            performance: 20,
            accessibility: 20,
            'best-practices': 20,
            seo: 10,
          },
          {
            formFactor: 'desktop',
            screenEmulation: {
              mobile: false,
              disable: false,
              width: Cypress.config('viewportWidth'),
              height: Cypress.config('viewportHeight'),
              deviceScaleRatio: 1,
            },
          },
        )
      })

      it.skip('Performance Feed types', () => {
        cy.visit('/feed-types')
       // cy.get(':nth-child(2) > .MuiListItemText-root > .MuiTypography-root').click()

        cy.lighthouse(
          {
            performance: 20,
            accessibility: 20,
            'best-practices': 20,
            seo: 10,
          },
          {
            formFactor: 'desktop',
            screenEmulation: {
              mobile: true,
              disable: false,
              width: Cypress.config('viewportWidth'),
              height: Cypress.config('viewportHeight'),
              deviceScaleRatio: 1,
            },
          },
        )
      })

      it('Performance Home page', () => {
        cy.visit('/')
      
        const thresholds = {
          performance: 50,
          accessibility: 90,
          'best-practices': 80,
          seo: 80,
        }
        const opts = {
          formFactor: 'desktop',
          screenEmulation: {
            mobile: false,
            disable: true,
            width: Cypress.config('viewportWidth'),
            height: Cypress.config('viewportHeight'),
            deviceScaleRatio: 1,
          },
          throttling: {
            rttMs: 150,
            throughputKbps: 1.6 * 1024,
            requestLatencyMs: 150 * DEVTOOLS_RTT_ADJUSTMENT_FACTOR,
            downloadThroughputKbps: 1.6 * 1024 * DEVTOOLS_THROUGHPUT_ADJUSTMENT_FACTOR,
            uploadThroughputKbps: 750 * DEVTOOLS_THROUGHPUT_ADJUSTMENT_FACTOR,
            cpuSlowdownMultiplier: 4,
          },
        }
        cy.url()
          .then((url) => {
            cy.task('lighthouse', {
              url,
              thresholds,
              opts,
            })
          })
          .then((report) => {
            const { errors, results, txt } = report
            // our custom code in the plugins file has summarized the report
            cy.log(report.txt)
          })
      })

      it('Performance Storages', () => {
        cy.visit('/storages')
      
        const thresholds = {
          performance: 50,
          accessibility: 90,
          'best-practices': 80,
          seo: 80,
        }
        const opts = {
          formFactor: 'desktop',
          screenEmulation: {
            mobile: false,
            disable: true,
            width: Cypress.config('viewportWidth'),
            height: Cypress.config('viewportHeight'),
            deviceScaleRatio: 1,
          },
          throttling: {
            rttMs: 40,
            throughputKbps: 10*1024,
            cpuSlowdownMultiplier: 1,
            requestLatencyMs: 0,
            downloadThroughputKbps: 0,
            uploadThroughputKbps: 0,
          },
        }
        cy.url()
          .then((url) => {
            cy.task('lighthouse', {
              url,
              thresholds,
              opts,
            })
          })
          .then((report) => {
            const { errors, results, txt } = report
            // our custom code in the plugins file has summarized the report
            cy.log(report.txt)
          })
      })

      it('Performance Feed types', () => {
        cy.visit('/feed-types')
      
        const thresholds = {
          performance: 50,
          accessibility: 90,
          'best-practices': 80,
          seo: 80,
        }
        const opts = {
          formFactor: 'desktop',
          screenEmulation: {
            mobile: false,
            disable: false,
            width: Cypress.config('viewportWidth'),
            height: Cypress.config('viewportHeight'),
            deviceScaleRatio: 1,
          },
        }
        cy.url()
          .then((url) => {
            cy.task('lighthouse', {
              url,
              thresholds,
              opts,
            })
          })
          .then((report) => {
            const { errors, results, txt } = report
            // our custom code in the plugins file has summarized the report
            cy.log(report.txt)
          })
      })
  })