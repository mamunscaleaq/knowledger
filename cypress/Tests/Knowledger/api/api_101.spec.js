/// <reference types="cypress" />

describe('example api tests', () => {
  before(() => {
    cy.postToken();
    cy.saveLocalStorage();
  })

  beforeEach(() => {
    cy.restoreLocalStorage();
  })
  it('test 1', () => {
    cy.getLocalStorage("identity_token").should("exist");
  
  })

  it("should exist identity in localStorage", () => {
    cy.getLocalStorage("identity_token").should("exist");
    cy.getLocalStorage("identity_token").then(token => {
      console.log("Identity token", token);
    });
  });

  it('get Activity by companyId', () => {
    cy.request({
        method : 'GET',
        url : `https://knowledger.api.scaleaq-dev.net/api/070D910C-2AE9-4B03-A448-8CD4B8E15EB0/aCTIVITY`,
        headers: {
            'authorization': 'Bearer '+localStorage.getItem('identity_token'),
          }
    }).then((res)=>{
        expect(res.status).to.eq(200)
        //expect(res.body[0].siloId).to.eq(870)
    })    
})  

  it.only('get user by id', () => {
    cy.request({
        method : 'GET',
        url : `https://knowledger.api.scaleaq-dev.net/api/070D910C-2AE9-4B03-A448-8CD4B8E15EB0/aCTIVITY`,
        headers: {
            'authorization': 'Bearer '+localStorage.getItem('identity_token'),
          }
    }).then((res)=>{
        expect(res.status).to.eq(200)
        //expect(res.body[0].siloId).to.eq(870)
    })    
}) 
 
})

