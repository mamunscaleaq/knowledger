/// <reference types="cypress" />

describe('FeedPlan api tests', () => {
  let KN;
    before(() => {
      cy.postToken();
      cy.saveLocalStorage();
    })
  
    beforeEach(() => {
      cy.restoreLocalStorage();
      cy.fixture("app_settings").then((app_settings) => {
        KN = app_settings;
      });
    })
  
    it('Get FeedPlan for a siteId', () => {
      cy.request({
          method : 'POST',
          url : Cypress.env('apiUrl')+KN.companyID+`/FeedPlan/GetFeedPlan`,
          headers: {
              'authorization': 'Bearer '+localStorage.getItem('identity_token'),
            },
         body: {
            "siteIds":["148"]
              }  
      }).then((res)=>{
          expect(res.status).to.eq(200)
          expect(res.body[0].siteId).to.eq('148')
          expect(res.body[0].plannedAmount).to.eq(11200)
      })    
  })

  it('Get FeedPlan for a siteId and Week', () => {
    cy.request({
        method : 'GET',
        url : Cypress.env('apiUrl')+KN.companyID+`/feedplan/week`,
        qs: {
            $siteId: 144,
            $year: 2022,
            $week: 16
        },
        headers: {
            'authorization': 'Bearer '+localStorage.getItem('identity_token'),
          },  
    }).then((res)=>{
        expect(res.status).to.eq(200)
        expect(res.body[0].planned).to.eq(5200)
    })    
})

it('Get FeedPlan for a siteId and Year', () => {
    cy.request({
        method : 'GET',
        url : Cypress.env('apiUrl')+KN.companyID+`/feedplan/year`,
        qs: {
            $siteId: 146,
            $year: 2022,
        },
        headers: {
            'authorization': 'Bearer '+localStorage.getItem('identity_token'),
          },  
    }).then((res)=>{
        expect(res.status).to.eq(200)
        expect(res.body.planned).to.eq(86800)
    })    
})

  
   
  })